package com.soukishop.souki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoukiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoukiApplication.class, args);
	}

}
